# -*- coding: utf-8 -*-
"""
@file    Lab0x02.py
@author  Fabian Enriquez
@brief   A program that measures the reaction time of a user when a LED turns on
@details This lab utilizes the STM32 microprocessor that incorparates 
         micropython. The program turns on an LED for 1 second and a user has 
         to push a button at the moment they see the light turn on. If done 
         successfully, then the program will return the average reaction time 
         and the amount of tries they took
@date    1/25/2021

source: https://bitbucket.org/wissurd/me405/src/master/Lab2/Lab0x02.py 
"""
import urandom 
import utime 
import pyb
import micropython

#Initializing
micropython.alloc_emergency_exception_buf(200)
##@brief Timer 5 with a prescale of 80 to get a resultion of 1us 
##@brief Timer 5 with a large period 
pyb.Timer(5,prescaler=79,period=0x7FFFFFFF)    

#LED pin PA5
##@brief LEDPin is connected to LD2 on the nucleo board
LEDPin = pyb.Pin(pyb.Pin.cpu.A5, mode=pyb.Pin.OUT_PP)
#Variables
##@brief isr_count is responible for tracking the amount of attempts the user took
isr_count= 0
##@brief total is the whole amount of reaction time that the user took
total = 0
##@brief avg is the average reaction time the user took
avg = 0
##@brief check is what inspects wheather the button has been pressed or not, 0 
#        means that no button has been pressed while 1 represents the opposite
check = 0

#Callback fucntion when interrupt occurs
def callback (event):   
    '''
    @brief when an external interrupt is detected, this function is then called upon 
    @details the callback function records the amount of time it took for the user to 
             react to a LED light, as well as the amount of attempts the user took. 
             
    @param event is a parameter which is ignored for the most part, only neeed
           when making a callback fucntion
    '''    
    global isr_count
    global check
    global duration
    duration = utime.ticks_diff(utime.ticks_us(), start_time)
    isr_count += 1
    check = 1
  
#External Interrupt  
##@brief External interrupt that is connected to pin 13 which is the user btn   
extint = pyb.ExtInt (pyb.Pin.board.PC13,     # Which pin  
           pyb.ExtInt.IRQ_FALLING,           # Interrupt on rising edge
           pyb.Pin.PULL_UP,                  # Activate pullup resistor
           callback)

#Instruction
print("When the light flashes, press the blue button to measure youre reaction time\nThen press CTRL-C to exit and see your avg time") 
pyb.delay(3000) #delay for 3 seconds to read inital message
# Main program loop
while True:
    try:
        check = 0                     # Checks to see if the user pressed a button, 0 is no, 1 is yes
        ##@brief this setups up the random time that this LED will turn on after 2 or 3 seconds
        setup = urandom.randrange(2,3)
        utime.sleep(setup)            # Delay for a random amount of time
        LEDPin.high()                 # Turn on LED
        ##@brief this starts a timer to measure the reaction of the user in microseconds
        start_time = utime.ticks_us() # Start timer count in microseconds                   
        pyb.delay(1000)               # Delay one second
        LEDPin.low()                  # Turn off LED
        
        if check == 0:
           print("You did not press any value")
        else: 
            total += duration/1000000

    except KeyboardInterrupt:
        break
    
#End Statement once user stops program
avg = total/(isr_count)
print("CTRL-C has been pressed")
print("Avg time (Seconds): ")    
print(avg)
print("Number of attempts: ")
print(isr_count)