## @file mainpage.py
#  Documentation for / use of mainpage.py
#
#  Detailed doc for mainpage.py 
#
#  @mainpage
#
#  @section sec_intro Portfolio Details 
#  This is my ME405 Portfolio. See individual modules for details.
#
#  Inluded modules are: 
#  *Lab0x00 (\ref sec_lab0)
#  *Lab0x01 (\ref sec_lab1)
#  *Lab0x02 (\ref sec_lab2)
#
#  @section sec_lab0 Lab0x00 Documentation
#  * Source: https://bitbucket.org/wissurd/me405/src/master/Lab1/HW0x01.py
#  * Documentation: \ref HW0x01
#
#  @section sec_lab1 Lab0x01 Documentation
#  * Source: https://bitbucket.org/wissurd/me405/src/master/Lab1/Lab0x01.py
#  * Documentation: \ref Lab0x01
#
#  @section sec_lab2 Lab0x02 Documentation
#  * Source: https://bitbucket.org/wissurd/me405/src/master/Lab2/Lab0x02.py 
#  * Documentation: \ref Lab0x02
#
#  @author Fabian Enriquez 
#
#  @copyright License Info
#
#  @date January 12, 2021
#