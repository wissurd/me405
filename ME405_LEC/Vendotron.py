# -*- coding: utf-8 -*-
"""
@file    Vendotron.py
@author  Fabian Enriquez
@brief   A program that asks a user to enter an amount of money and choose a 
         beverage.
@details The program asks a user to enter the amount
         they have in the form of coins/bills, then once they are ready to 
         choose a drink, they can at anytime which will process if it is 
         sufficient funds and return the change.
@copyright
@date    1/15/2021
"""

import keyboard
import HW0x01


penny = 0    
nickel = 0
dime = 0
quarter = 0
dollar = 0
five = 0
ten = 0
twenty = 0
price = 0
payment = (penny, nickel, dime, quarter, dollar, five, ten, twenty)
newchange = (0,0,0,0,0,0,0,0)
state = 0
pushed_key = None
 

def printWelcome():
    '''
    @brief a function that prints a welcome message while the user enters their payment
    @details this functions purpose only prints a message at the beginnging of state 0
             during the initialization 
    '''
    print("Welcome, Try Coke Today!")


def on_keypress (thing):
    '''
    @brief checks if a user pressed down on any button 
    @details the function will record the value of the button pressed and 
    will return it as a global parameter
    
    @param pushed_key is the value that is recorded when the button is pressed 
    '''
    global pushed_key
    pushed_key = thing.name

    
   
   
keyboard.on_press (on_keypress)  ########## Set callback
   
while True: 
    """
    Implement FSM using a while loop and an if statement 
    will run infinitely until user presses CTRL-C
    """ 
    try:          
        if state == 0: #INIT state
            printWelcome()
            state = 1    
            
        elif state == 1: #Gets Coins
            #print("In state one!", pushed_key)
            if pushed_key:
                if pushed_key == "0":
                    penny += 1
                    print ("This is worth a cent")
                    state = 1
 
                elif pushed_key == '1':
                    nickel += 1
                    print ("Non-wooden nickel detected")
                    state = 1
 
                elif pushed_key == '2':  
                    dime += 1
                    print ("We're up to our {:d}th dime".format (dime))
                    state = 1
 
                elif pushed_key == '3':    
                    quarter += 1
                    print ("Quarters selected")
                    state = 1
                     
                elif pushed_key == '4':                      
                    dollar += 1
                    print ("dolla")
                    state = 1
                     
                elif pushed_key == '5':               
                    five += 1
                    print ("five dolla")
                    state = 1
                     
                elif pushed_key == '6':                  
                    ten += 1
                    print ("ten")
                    state = 1
                     
                elif pushed_key == '7':      
                    twenty += 1
                    print ("twenty dollar")
                    state = 1
                 
                elif pushed_key == 'c':
                    state = 2
                    price = 1.00
                    print("Coke has been selected")
                     
                elif pushed_key == 'p':
                    state = 2
                    price = 1.20
                    print("Pepsi has been selected")
                 
                elif pushed_key == 's':
                    state = 2
                    price = 0.85
                    print("Sprite has been selected")
             
                elif pushed_key == 'd':
                    state = 2
                    price = 1.10
                    print("Dr.Pepper has been selected")
                 
                elif pushed_key == 'e':
                    payment = (0,0,0,0,0,0,0,0)
                    print('Ejected')
                    state = 0
                payment = (penny, nickel, dime, quarter, dollar, five, ten, twenty)                       
                pushed_key = None
        
        
        elif state == 2: #compute change
            newchange = HW0x01.getChange(price,payment)
            penny = newchange[0]
            nickel = newchange[1]
            dime = newchange[2]
            quarter = newchange[3]
            dollar = newchange[4]
            five = newchange[5]
            ten = newchange[6]
            twenty = newchange[7]
            state = 0
     
     
        else: 
           """
           User should not be able to go to this state,
           otherwise something is wrong
           """
           pass
    
    except KeyboardInterrupt:
        break
print ("Control-C has been pressed, so it's time to exit.")
keyboard.unhook_all ()  

if __name__ == "__main__":  
    pass