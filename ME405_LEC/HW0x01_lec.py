# -*- coding: utf-8 -*-
"""
@file    HW0x01.py
@author  Fabian Enriquez
@brief   A program that returns change given a price and a payment
@details The program asks a user to enter the price of an item and the amount
         they have, the function then computes the change and returns it in the 
         least amount of bills/coins in the form of a tuple
@copyright
@date    1/11/2021
"""
#Vending Machine
#payment = input("Enter the amount you have: $")
#price1 = input("What is the price of the item: $")
#price1 = float(price1)
#payment = float(payment)


##@breif a tuple that is returned after the getChange function has been called
# @details the tuple consist of the same order as the payment tuple which 
#          starts from pennies in acending order to twenty dollar bills
newchange = (0,0,0,0,0,0,0,0)

#automatically convert the user payment into a tuple
# payment = payment*100
# twnty = payment//2000
# if twnty < 1:
#     twnty = 0
# payment %= 2000
# ten = payment//1000
# if ten < 1:
#     ten = 0
# payment %= 1000
            
# five = payment//500
# if five < 1:
#     five = 0
# payment %= 500
            
# dollar = payment//100      
# if dollar < 1:
#     dollar = 0
# payment %= 100
            
# quart = payment//25
# if quart < 1:
#     quart = 0
# payment %= 25
            
# dime = payment//10
# if dime < 1:
#     dime = 0
# payment %= 10
            
# nickl = payment//5
# if nickl < 1:
#     nickl = 0
# payment %= 5
            
# penni = payment//1
# if penni < 1:
#         penni = 0
        
# payment = (penni,nickl,dime,quart,dollar,five,ten,twnty) 
   
    
#the get change function
def getChange(price, payment):
    '''
        @brief a function that returns the change depending on a price and payment
        @details the get change functions takes in a price and a payment, then 
                 checks if the transaction is sufficient to return the least amount
                 of change in the form of bills/coins, if not sufficient then return
                 nothing
        @param price is given in the form of an interger
               payment is given in the form of a tuple that accends in order starting
               from a penny to twenty dollar bills
        @return a tuple is return which contains the resulting change in bills and coins
    '''
    total = (payment[0]*0.01) +(payment[1]*0.05)+(payment[2]*0.10)+\
        (payment[3]*0.25) +(payment[4]*1)+(payment[5]*5) + \
        (payment[6]*10)+(payment[7]*20)
        
    print('The total amount of money you entered is $' + str(total) +\
          ' and the price of the item is $'+ str(price))
    
    if price > total:
        print('Sorry, there are no sufficient funds to proceed with the purchase')
        newchange = payment
    else:
        change = (total*100) - (price*100)
        change = change/100
        print('your change is $'+str(change))
        if change == 0:
            print('Nooice')
            newchange = (0,0,0,0,0,0,0,0)
        else: 
            change = change *100
            twnty = change//2000
            if twnty < 1:
                twnty = 0
            change %= 2000
            
            ten = change//1000
            if ten < 1:
                ten = 0
            change %= 1000
            
            five = change//500
            if five < 1:
                five = 0
            change %= 500
            
            dollar = change//100      
            if dollar < 1:
                dollar = 0
            change %= 100
            
            quart = change//25
            if quart < 1:
                quart = 0
            change %= 25
            
            dime = change//10
            if dime < 1:
                dime = 0
            change %= 10
            
            nickl = change//5
            if nickl < 1:
                nickl = 0
            change %= 5
            
            penni = change//1
            if penni < 1:
                penni = 0
            print('your total change is '+str(twnty)+' twenty, '+str(ten)+' ten, '+str(five)+' five, '+str(dollar)+' dollar, '+\
              str(quart)+ ' quarters, '+str(dime)+' dimes, '+str(nickl)+' nickels, '+str(penni)+' pennies')
            newchange = (penni,nickl,dime,quart,dollar,five,ten,twnty)
    return newchange

#output 
#newchange = getChange(price1,payment1)  

#test
if __name__ == "__main__":
    print("Your new tuple is:")
    print(newchange)
    
    
    


